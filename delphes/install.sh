#! /bin/bash

# Install Delphes
git clone https://github.com/delphes/delphes.git --depth 1 # This will create a folder called delphes
cd delphes # You might want to change the version of Delphes you are installing, or delete the .git folder after installation
make -j 4 # This will compile Delphes