#! /bin/bash
# Install MadGraph5_aMC@NLO (and Pythia8 and LHAPDF6...)
wget https://launchpad.net/mg5amcnlo/3.0/3.5.x/+download/MG5_aMC_v2.9.19.tar.gz
tar -xvzf MG5_aMC_v2.9.19.tar.gz # This will create a folder called MG5_aMC_v2_9_19
rm -rf MG5_aMC_v2.9.19.tar.gz
cd MG5_aMC_v2_9_19
echo 'install pythia8' | ./bin/mg5_aMC # This is really redundant, but it's good to have it!
echo 'install lhapdf6' | ./bin/mg5_aMC # This is really redundant, but it's good to have it!