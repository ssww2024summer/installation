#! /bin/bash

# Install MadAnalysis5
wget https://github.com/MadAnalysis/madanalysis5/archive/refs/tags/v1.10.12.tar.gz
tar -xvzf v1.10.12.tar.gz # This will create a folder called madanalysis5-1.10.12
rm -rf v1.10.12.tar.gz
cd madanalysis5-1.10.12
sed -i 's/python/python3/g' bin/ma5
echo "install delphesMA5tune" | ./bin/ma5 -s 
# echo "install delphesMA5tune\n4" | ./bin/ma5 # This will install MadAnalysis5 with 4 cores