# Installation instructions for the summer project on SSWW recasting

## Tools to be used

1. MadGraph_aMCatNLO (will have to use 273, closet to CMS analysis...!)
2. Pythia8, LHAPDF6... (will be handled by MadGraph for simplicity)
3. Delphes (will be installed by hand since there might be some issues...)
4. Madanalysis5 (will be installed by hand)

### Optional
5. Use packages from `LCG` (explain later)
6. CMSSW (experiencing MC with CMSSW)
7. HiggsCombineTool
8. Use more recent version of MG5... 

## Basic setup
1. Using `lxplus9`
2. Clone this repo 
```
git clone https://gitlab.cern.ch/ssww2024summer/installation.git
```
3. go to this repo and install
```
cd installation
cd <package name>
./install
```
